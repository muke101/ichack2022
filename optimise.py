import random
import json
import requests
import math

class Emergency:
    def __init__(self, lon, lat, weight):
        self.lon = lon
        self.lat = lat
        self.weight = weight

class Coord:
    def __init__(self, lon, lat, score):
        self.lon = lon
        self.lat = lat
        self.score = score


def get_neighbors(lon, lat, step):
    return [(lon+step, lat), (lon, lat+step), (lon+step, lat+step),
            (lon-step, lat), (lon, lat-step), (lon-step, lat-step),
            (lon+step, lat-step), (lon-step, lat+step)]

def find_optimal_position(emergencies, min_lon, min_lat, max_lon, max_lat, step):
    start_lon = random.uniform(min_lon, max_lon)
    start_lat = random.uniform(min_lat, max_lat)
    visited = set()
    visited.add((start_lon, start_lat))
    maximum_found = False
    while maximum_found == False:
        initial_score = score(emergencies, start_lon, start_lat)
        coords = get_neighbors(start_lon, start_lat, step)
        max_score = 0
        max_coord = Coord(None, None, None) # making my lsp shut up
        for lon, lat in coords:
            if (lon, lat) not in visited:
                neighbor_score = score(emergencies, lon, lat)
                if max_score < neighbor_score:
                    max_coord = Coord(lon, lat, neighbor_score)
                    max_score = neighbor_score
                visited.add((lon, lat))
        if max_score < initial_score:
            maximum_found = True
            break
        start_lon = max_coord.lon
        start_lat = max_coord.lat
    return (start_lon, start_lat)

def calculateTime(latOrg, lonOrg, latDes, lonDes):
    #url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+ latOrg +"%2C"+ lonOrg + ",&destinations="+latDes+"%2C"+lonDes+"&key=AIzaSyCwBluVddK8b8h3fCTxWUD77i7L2Tep9ew"

    payload={}
    headers = {}

    # would use google maps API but will end up charging hundreds of pounds for our demo

    #response = requests.request("GET", url, headers=headers, data=payload)
    #j = json.loads(response.text)
    #return j["rows"][0]["elements"][0]["duration"]["value"]

    s = 13.6
    r = 6378137
    lat_diff = latDes - latOrg
    lon_diff = lonOrg - lonDes
    a = math.sin(lat_diff/2)*math.sin(lat_diff/2) + math.cos(latOrg) * math.cos(latDes) * math.sin(lon_diff / 2) * math.sin(lon_diff / 2)
    d = r * 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    return d / s

def score(emergencies, start_lon, start_lat):
    score = 0
    for emergency in emergencies:
        dest_lon = emergency.lon
        dest_lat = emergency.lat
        time = calculateTime(start_lat, start_lon, dest_lat, dest_lon)
        score += emergency.weight / time
        emergency.weight += time
    return score

if __name__ == "__main__":
    import csv
    test_data = open("./data/Friday1.csv", 'r')
    reader = csv.reader(test_data)
    emergencies = []
    min_lon = 51.48824575
    min_lat = -0.119901205
    max_lon = 51.5070915
    max_lat = -0.110788213
    step = 0.00001
    for row in reader:
        emergencies.append(Emergency(float(row[2]), float(row[3]), 1))
    for a in range(10):
        opt_lon, opt_lat = find_optimal_position(emergencies, min_lon, min_lat, max_lon, max_lat, step)
        print(opt_lon, opt_lat)
