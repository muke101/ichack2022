from random import randint
import csv

import numpy

# csv header
fieldnames = ['time', 'day', 'lng', 'lat']

# csv data
rows = []
# Change the file name
with open('Sunday4.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f)
    for i in range(1000):
        value = randint(0, 23)
        value1 = randint(0, 59)
        time = str(value) + ':' + str(value1)
        # generate random float between [min_lng, max_lng)
        val1 = numpy.random.uniform(51.488236, 51.507110)
        # generate random float between [min_lat, max_lat)
        val2 = numpy.random.uniform(-0.119906, -0.080764)
        # Change the name of the week
        data = [time, 'Sunday', val1, val2]
        writer.writerow(data)

f.close()
