import pandas as pd
import numpy as np

def convertCsvToJson(filePath):
    df = pd.read_csv(filePath, encoding = 'latin1', header=None)
    data = df.to_numpy()
    return data

data = convertCsvToJson('data.csv')
print(data)
