import csv
from sklearn.mixture import GaussianMixture
from sklearn.cluster import KMeans
import time
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def predict_new_day(filename):
    data = csv.reader(open("data/"+filename, 'r'))
    times = []
    lons = []
    lats = []
    for row in data:
        hour, minute = row[0].split(":")
        if len(hour) == 1: hour = "0"+hour
        if len(minute) == 1: minute = "0"+minute
        times.append(time.mktime(time.strptime("2022-02-20 "+hour+":"+minute+":"+"00", "%Y-%m-%d %H:%M:%S")))
        lons.append(float(row[2]))
        lats.append(float(row[3]))
    times_x = np.linspace(0, times[-1], len(times))
    lons_x = np.linspace(0, times[-1], len(lons))
    lats_x = np.linspace(0, times[-1], len(lats))
    times = [i for i in zip(times_x, times)]
    lons = [i for i in zip(lons_x, lons)]
    lats = [i for i in zip(lats_x, lats)]
    time_model = GaussianMixture(n_components=5, random_state=0).fit(times)
    lon_model = GaussianMixture(n_components=5, random_state=0).fit(lons)
    lat_model = GaussianMixture(n_components=5, random_state=0).fit(lats)

    new_times = [t[1] for t in time_model.sample(1000)[0]]
    new_lons = [l[1] for l in lon_model.sample(1000)[0]]
    new_lats = [l[1] for l in lat_model.sample(1000)[0]]

predict_new_day("Friday1.csv")
# data = csv.reader(open("data/Friday1.csv", 'r'))
# data_friday = pd.read_csv("data/Friday1.csv")

# def predict_new_day(filename):
#     data_all = pd.read_csv(filename)

#     # features = data_all[['time', 'day', 'week', 'lat', 'long']]
#     features_encoded = pd.get_dummies(data_all, columns=["day"])[['time', 'week', 'lat', 'long',
#                                                                   'day_Monday', 'day_Tuesday',
#                                                                   'day_Wednesday', 'day_Thursday',
#                                                                   'day_Friday', 'day_Saturday',
#                                                                   'day_Sunday']].values
#     # times = []
#     for i in range(len(features_encoded)):
#         hour, minute = features_encoded[i][0].split(":")
#         print(hour, minute)
#         # if len(hour) == 1: hour = "0"+hour
#         # if len(minute) == 1: minute = "0"+minute
#         features_encoded[i][0] = time.mktime(time.strptime("2022-02-20 "+hour+":"+minute+":"+"00", "%Y-%m-%d %H:%M:%S"))
#         # lons.append(float(row[2]))
#         # lats.append(float(row[3]))


#     # sse = {}
#     # for k in range(1, 20):
#     #     kmeans = KMeans(n_clusters=k, max_iter=1000).fit(features_encoded)
#     #     # features_encoded["clusters"] = kmeans.labels_
#     #     #print(features_encoded["clusters"])
#     #     sse[k] = kmeans.inertia_ # Inertia: Sum of distances of samples to their closest cluster center

#     # plt.figure()
#     # plt.plot(list(sse.keys()), list(sse.values()))
#     # plt.xlabel("Number of cluster")
#     # plt.ylabel("SSE")
#     # plt.show()


#     # Create KMeans model and fit to data with 5 clusters
#     kmeans = KMeans(n_clusters=5)
#     clusters = kmeans.fit_predict(features_encoded)

#     data_all["label"] = clusters
#     data_all.to_csv("data/input_clustered.csv", index=False)


# predict_new_day("data/input.csv")
def predict_new_day(filename):
    data_all = pd.read_csv(filename)
    
    # features = data_all[['time', 'day', 'week', 'lat', 'long']]
    features_encoded = pd.get_dummies(data_all, columns=["day"])[['lat', 'long']].values
    
    # times = []
    # for i in range(len(features_encoded)):
    #     hour, minute = features_encoded[i][0].split(":")
    #     print(hour, minute)
    #     # if len(hour) == 1: hour = "0"+hour
    #     # if len(minute) == 1: minute = "0"+minute
    #     features_encoded[i][0] = time.mktime(time.strptime("2022-02-20 "+hour+":"+minute+":"+"00", "%Y-%m-%d %H:%M:%S"))
    #     # lons.append(float(row[2]))
    #     # lats.append(float(row[3]))
    
    
    # sse = {}
    # for k in range(1, 20):
    #     kmeans = KMeans(n_clusters=k, max_iter=1000).fit(features_encoded)
    #     # features_encoded["clusters"] = kmeans.labels_
    #     #print(features_encoded["clusters"])
    #     sse[k] = kmeans.inertia_ # Inertia: Sum of distances of samples to their closest cluster center
        
    # plt.figure()
    # plt.plot(list(sse.keys()), list(sse.values()))
    # plt.xlabel("Number of cluster")
    # plt.ylabel("SSE")
    # plt.show()
    
    
    # Create KMeans model and fit to data with 5 clusters
    kmeans = KMeans(n_clusters=5)
    clusters = kmeans.fit_predict(features_encoded)
    print(kmeans.cluster_centers_)
    
    data_all["label"] = clusters
    data_all.to_csv("data/input_clustered.csv", index=False)
    
    
predict_new_day("data/input.csv")
